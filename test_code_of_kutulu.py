from code_of_kutulu import *

def test_map_row():
    game_map = Map("20 30")
    input_string = "##........w.......##"
    game_map.read_line(input_string,0)
    assert(game_map.field[0][0]  == "WALL")
    assert(game_map.field[0][1]  == "WALL")
    assert(game_map.field[0][18] == "WALL")
    assert(game_map.field[0][19] == "WALL")
    assert(game_map.field[0][10] == "WANDERER_SPAWN")

def test_read_map_size():
    val = "20 30"
    game_map = Map(val)
    assert(game_map.width == 20)
    assert(game_map.height == 30)

def test_entity():
    explorer = Entity("EXPLORER 0 7 7 250 2 3")
    assert(explorer.entity_type == "EXPLORER")
    assert(explorer.id == 0)
    assert(explorer.position == Position(7,7))

def test_entity_count():
    entities = EntityList("20")
    assert(len(entities) == 20)
