class EntityList(list):
    def __init__(self,string):
        self._data = [Entity() for i in range(int(string))]

    def __len__(self):
        return len(self._data)

class Position:
    def __init__(self,x,y):
        self.x = x
        self.y = y

    def __eq__(self,other):
        return self.x == other.x and self.y == other.y

    def __ne__(self,other):
        return self.x != other.x or self.y != other.y

class Entity:
    def __init__(self,entity_string=""):
        if entity_string is "": return
        self.entity_type,id,x,y,_,_,_ = [s for s in entity_string.split()]
        self.id = int(id)
        self.position = Position(int(x),int(y))

class Map:
    def __init__(self, map_string):
        self.width, self.height = [int(i) for i in map_string.split()]
        self.field = [[0 for x in range(self.width)] for y in range(self.height)]

    def read_line(self,row,row_id):
        for i,fld in enumerate(row):
            self.field[row_id][i] = ''
            if fld == '#':
                self.field[row_id][i] = 'WALL'
            elif fld == 'w':
                self.field[row_id][i] = 'WANDERER_SPAWN'
